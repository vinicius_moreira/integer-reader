package moreira.vinicius.intreader.search;

import java.util.List;

/**
 * @author vinicius.moreira
 */
public interface SearchAlgorithm {

	/**
	 * It finds a target integer within an array and returns its index
	 * @param integers
	 * @param integer
	 * @return
	 * 		the element index or -1 if the integer is not present.
	 * @throws NullPointerException
	 * 		when the arguments are null
	 * @throws IllegalArgumentException
	 * 		when the list is empty
	 */
	int findIndex(List<Integer> integers, Integer integer) throws NullPointerException, IllegalArgumentException;
}
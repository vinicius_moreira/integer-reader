package moreira.vinicius.intreader.search;

import java.util.List;

import org.apache.commons.lang3.Validate;

/**
 * @author vinicius.moreira
 */
public class BinarySearchAlgorithm implements SearchAlgorithm {

	@Override
	public int findIndex(List<Integer> integers, Integer integer) throws NullPointerException, IllegalArgumentException {
		Validate.notEmpty(integers);
		Validate.notNull(integer);

		return this.execute(integers, integer, 0, integers.size() - 1);
	}

	private int execute(List<Integer> integers, int target, int idxMin, int idxMax) {
		if (idxMax < idxMin)
			return -1;
		else {
			// calculate midpoint to cut set in half
			int idxMid = calculateMidPoint(idxMin, idxMax);

			Integer midElement = integers.get(idxMid);

			if (midElement > target) { // key is in lower subset
				return execute(integers, target, idxMin, idxMid - 1);
			} else if (midElement < target) { // key is in upper subset
				return execute(integers, target, idxMid + 1, idxMax);
			} else { // target has been found
				return idxMid;
			}
		}
	}

	private int calculateMidPoint(int idxMin, int idxMax) {
		return idxMin + ((idxMax - idxMin) / 2);
	}

}
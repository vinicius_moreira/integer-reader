package moreira.vinicius.intreader.sort;

import java.util.List;

import org.apache.commons.lang3.Validate;

/**
 * @author vinicius.moreira
 */
public class QuickSort implements SortAlgorithm {

	@Override
	public void execute(List<Integer> ints) throws NullPointerException {
		Validate.notEmpty(ints);

		if (ints.size() < 2) {
			return;
		}

		this.execute(ints, 0, ints.size() - 1);
	}

	private void execute(List<Integer> ints, int lowIdx, int highIdx) {

		if (ints.size() == 0 || (lowIdx > highIdx)) {
			return;
		}

		int pivot = this.getPivot(ints, lowIdx, highIdx);

		// making everything on the left < pivot and on the right > pivot
		int left = lowIdx, right = highIdx;

		while (left <= right) {
			while (ints.get(left) < pivot) {
				left++;
			}

			while (ints.get(right) > pivot) {
				right--;
			}

			if (left <= right) {
				int temp = ints.get(left);
				ints.set(left, ints.get(right));
				ints.set(right, temp);
				left++;
				right--;
			}
		}

		// sorting the two sub parts recursively
		
		if (lowIdx < right) {
			execute(ints, lowIdx, right);
		}

		if (highIdx > left){
			execute(ints, left, highIdx);
		}

	}

	private int getPivot(List<Integer> ints, int lowIdx, int highIdx) {
		int pivotPosition = lowIdx + (highIdx - lowIdx) / 2;
		return ints.get(pivotPosition);
	}
}
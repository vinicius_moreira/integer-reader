package moreira.vinicius.intreader.sort;

import java.util.List;

/**
 * @author vinicius.moreira
 */
public interface SortAlgorithm {

	/**
	 * @param ints
	 * @throws NullPointerException
	 * 		when the argument is null
	 * @throws IllegalArgumentException
	 * 		when the list is empty
	 */
	void execute(List<Integer> ints) throws NullPointerException, IllegalArgumentException;
}
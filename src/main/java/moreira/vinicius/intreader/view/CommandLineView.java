package moreira.vinicius.intreader.view;

import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.Validate;

import moreira.vinicius.intreader.controller.FileReadingController;
import moreira.vinicius.intreader.controller.IntegerSearchController;

/**
 * @author vinicius.moreira
 */
public class CommandLineView {
	
	private static final String APP_MSG_PREFIX = "[integer-reader] ";

	private Scanner scanner;
	private FileReadingController fileReadingController;
	private IntegerSearchController integerSearchController;
	private String fileName;
	private List<Integer> integersRead;
	private Set<Integer> menuComands;
	
	public CommandLineView(String fileName, FileReadingController fileReadingController, IntegerSearchController integerSearchController) throws NullPointerException, IllegalArgumentException {
		Validate.notNull(fileReadingController);
		Validate.notNull(integerSearchController);
		Validate.notBlank(fileName);
		this.fileReadingController = fileReadingController;
		this.integerSearchController = integerSearchController;
		this.fileName = fileName;
		this.scanner = new Scanner(System.in);
	}
	
	public void initialize(){
		this.renderMessage("Loading...");
		this.readIntegersFromFile();
		this.renderMenu();
	}

	private void readIntegersFromFile() {
		renderMessage("");
		renderMessage("Trying to read the file from classpath");
		this.integersRead = this.fileReadingController.readFile(this.fileName);
		renderMessage("Integers found: "+this.integersRead);
		renderMessage("");
	}
	
	private void renderMenu(){
		this.menuComands = new TreeSet<>();
		this.menuComands.add(0);
		this.menuComands.add(1);
		
		while(true){
			renderMessage("...........................");
			renderMessage("....       MENU        ....");
			renderMessage("...........................");
			renderMessage(".. [0] - Verify a number ..");
			renderMessage(".. [1] - Exit            ..");
			int command = this.waitForValidMenuCommand();
			this.callMenuCommand(command);
		}
	}
	
	private int waitForValidMenuCommand(){
		while(true){
			try{
				String command = this.scanner.next();
				Integer intCommmand = Integer.valueOf(command);
				
				if(this.menuComands.contains(intCommmand)){
					return intCommmand;
				}
			}
			catch(NumberFormatException e){
				this.renderMessage("You typed a wrong command, please try again");
			}
		}
	}
	
	private void callMenuCommand(int command){
		switch(command){
		case 0 :{
			this.executeIntegerSearch();
			break;
		}
		case 1: {
			this.exitProgram();
		}
		default :{
			this.exitProgram();
		}
		}
	}
	
	private void renderMessage(String msg){
		System.out.println(APP_MSG_PREFIX + msg);
	}
	
	private void executeIntegerSearch(){
		Integer integer = this.askForInteger();
		this.renderMessage("Verifying if ["+integer+"] exists");
		boolean contains = this.integerSearchController.contains(integer, this.integersRead);
		this.renderMessage(String.valueOf(contains));
	}
	
	private Integer askForInteger(){
		while(true){
			try {
				this.renderMessage("Which integer do you want to verify ?");
				String stringInt = this.scanner.next();
				Integer parsedInt = Integer.valueOf(stringInt);
				return parsedInt;
			}
			catch(NumberFormatException e){
				this.renderMessage("You typed wrong. Please, type again...");
			}
		}
	}
	
	private void exitProgram(){
		this.renderMessage("Exiting...");
		System.exit(0);
	}
}
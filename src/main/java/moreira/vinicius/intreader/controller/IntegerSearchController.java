package moreira.vinicius.intreader.controller;

import java.util.List;

import org.apache.commons.lang3.Validate;

import moreira.vinicius.intreader.search.SearchAlgorithm;

/**
 * @author viniciu.moreira
 */
public class IntegerSearchController {

	private SearchAlgorithm searchAlgorithm;

	public IntegerSearchController(SearchAlgorithm searchAlgorithm) throws NullPointerException {
		Validate.notNull(searchAlgorithm);
		this.searchAlgorithm = searchAlgorithm;
	}
	
	public boolean contains(Integer targetInteger, List<Integer> integers){
		return this.searchAlgorithm.findIndex(integers, targetInteger) != -1;
	}
}
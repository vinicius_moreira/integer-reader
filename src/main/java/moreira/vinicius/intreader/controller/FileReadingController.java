package moreira.vinicius.intreader.controller;

import java.util.List;

import org.apache.commons.lang3.Validate;

import moreira.vinicius.intreader.file.IntegersFileReader;

/**
 * @author vinicius.moreira
 */
public class FileReadingController {

	private IntegersFileReader fileReader;
	
	public FileReadingController(IntegersFileReader fileReader) throws NullPointerException {
		Validate.notNull(fileReader);
		this.fileReader = fileReader;
	}

	public List<Integer> readFile(String fileName) throws NullPointerException, IllegalArgumentException {
		Validate.notBlank(fileName);
		return this.fileReader.read(fileName);
	}
}
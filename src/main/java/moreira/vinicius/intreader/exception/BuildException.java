package moreira.vinicius.intreader.exception;

/**
 * {@link RuntimeException} thrown when a builder was not able to build a new instance.
 * @author vinicius.moreira
 */
public class BuildException extends RuntimeException {

	/**	 */
	private static final long serialVersionUID = -1345045114162914277L;
	
	public BuildException(String msg) {
		super(msg);
	}
}
package moreira.vinicius.intreader.file;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

/**
 * @author vinicius.moreira
 */
public class IntegersStringReader {

	/**
	 * @param str
	 * @return
	 * @throws NullPointerException
	 * 		when the argument is null
	 * @throws IllegalArgumentException
	 * 		when the argument is blank
	 */
	List<Integer> read(String str) throws NullPointerException, IllegalArgumentException, NumberFormatException {
		Validate.notBlank(str);
		
		List<Integer> ints = new ArrayList<Integer>();
		
		String[] splitInts = str.trim().split(" ");
		
		for(String intString : splitInts){
			ints.add(Integer.valueOf(intString));
		}
		
		return ints;
	}
}
package moreira.vinicius.intreader.file;

import java.util.List;

import moreira.vinicius.intreader.sort.SortAlgorithm;

import org.apache.commons.lang3.Validate;

/**
 * Implemented based on Decorator (Wrapper) Pattern. Basically, just order the result provided by another implementation of {@link IntegersFileReader} provided.
 * 
 * @author vinicius.moreira
 */
public class OrderedIntegersFileReader implements IntegersFileReader {

	private IntegersFileReader innerReader;
	private SortAlgorithm sortAlgorithm;
	
	/**
	 * @param innerReader
	 * @throws NullPointerException
	 * 		when the argument is null
	 */
	public OrderedIntegersFileReader(IntegersFileReader innerReader, SortAlgorithm sortAlgorithm) throws NullPointerException {
		Validate.notNull(innerReader);
		Validate.notNull(sortAlgorithm);
		this.innerReader = innerReader;
		this.sortAlgorithm = sortAlgorithm;
	}

	@Override
	public List<Integer> read(String filePath) throws NullPointerException, IllegalArgumentException, InvalidIntegersFileException {
		
		List<Integer> integers = this.innerReader.read(filePath);
		this.sortAlgorithm.execute(integers);
		
		return integers;
	}
	
	public SortAlgorithm getSortAlgorithm(){
		return this.sortAlgorithm;
	}
	
	public IntegersFileReader getInnerReader(){
		return this.innerReader;
	}
}
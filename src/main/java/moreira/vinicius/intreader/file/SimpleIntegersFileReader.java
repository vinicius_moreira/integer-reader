package moreira.vinicius.intreader.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

/**
 * @author vinicius.moreira
 */
public class SimpleIntegersFileReader implements IntegersFileReader {

	private IntegersStringReader stringReader;
	
	public SimpleIntegersFileReader() {
		this.stringReader = new IntegersStringReader();
	}
	
	public List<Integer> read(String filePath) throws NullPointerException, IllegalArgumentException {
		Validate.notBlank(filePath);
		
		
		List<Integer> integers = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/"+filePath))))
		{

			String currentLine;

			while ((currentLine = br.readLine()) != null) {
				List<Integer> lineIntegers = this.stringReader.read(currentLine);
				// HOOK method that can be changed by subclasses
				this.addCurrentLine(lineIntegers, integers);
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		catch(NumberFormatException e){
			throw new InvalidIntegersFileException();
		}
		
		return integers;
	}
	
	protected void addCurrentLine(List<Integer> currentLine, List<Integer> allRead){
		allRead.addAll(currentLine);
	}
}
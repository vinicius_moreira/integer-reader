package moreira.vinicius.intreader.file;

import java.util.List;

/**
 * @author vinicius.moreira
 */
public interface IntegersFileReader {

	/**
	 * @param filePath
	 * @return
	 * @throws NullPointerException
	 * 		when the argument is null
	 * @throws IllegalArgumentException
	 * 		when the argument is blank
	 * @throws InvalidIntegersFileException
	 * 		when the file contain invalid characters
	 */
	List<Integer> read(String filePath) throws NullPointerException, IllegalArgumentException, InvalidIntegersFileException;
}
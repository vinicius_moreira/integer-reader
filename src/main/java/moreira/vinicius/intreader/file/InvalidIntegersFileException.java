package moreira.vinicius.intreader.file;

/**
 * {@link RuntimeException} thrown when a supposed integers file contain invalid data (such as letters, symbols, ...)
 * 
 * @author vinicius.moreira
 */
public class InvalidIntegersFileException extends RuntimeException {

	/**	 */
	private static final long serialVersionUID = -3576650694516761403L;

}
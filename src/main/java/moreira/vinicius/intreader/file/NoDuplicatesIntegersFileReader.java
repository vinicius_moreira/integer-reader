package moreira.vinicius.intreader.file;

import java.util.List;

/**
 * It inherits all the behaviors of {@link SimpleIntegersFileReader}, but just returns different elements.
 *  
 * @author vinicius.moreira
 */
public class NoDuplicatesIntegersFileReader extends SimpleIntegersFileReader {

	@Override
	protected void addCurrentLine(List<Integer> currentLine, List<Integer> allRead) {
		
		for(Integer currentLineInteger : currentLine){
			if(!allRead.contains(currentLineInteger)){
				allRead.add(currentLineInteger);
			}
		}
	}
}
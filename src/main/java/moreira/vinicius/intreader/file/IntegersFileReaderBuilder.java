package moreira.vinicius.intreader.file;

import org.apache.commons.lang3.Validate;

import moreira.vinicius.intreader.exception.BuildException;
import moreira.vinicius.intreader.sort.SortAlgorithm;

/**
 * @author vinicius.moreira
 */
public class IntegersFileReaderBuilder {

	private IntegersFileReader reader;
	
	/**
	 * @return
	 * @throws BuildException
	 */
	public IntegersFileReader build() throws BuildException {
		this.validate();
		return this.reader;
	}
	
	public IntegersFileReaderBuilder simple(){
		this.reader = new SimpleIntegersFileReader();
		return this;
	}
	
	public IntegersFileReaderBuilder withNoDuplicates(){
		
		IntegersFileReader previousReader = this.reader;
		
		this.reader = new NoDuplicatesIntegersFileReader();
		
		if(previousReader instanceof OrderedIntegersFileReader){
			this.ordered(((OrderedIntegersFileReader) previousReader).getSortAlgorithm());
		}
		
		return this;
	}
	
	public  IntegersFileReaderBuilder ordered(SortAlgorithm algorithm) throws NullPointerException, BuildException {
		Validate.notNull(algorithm);
		
		if(this.reader instanceof OrderedIntegersFileReader){
			throw new BuildException("You have already defined that you reader must be ordered !");
		}
		
		if(this.reader == null){
			this.simple();
		}
		
		this.reader = new OrderedIntegersFileReader(this.reader, algorithm);
		return this;
	}
	
	private void validate(){
		if(this.reader == null){
			throw new BuildException("You have not defined any type of reader");
		}
	}
}
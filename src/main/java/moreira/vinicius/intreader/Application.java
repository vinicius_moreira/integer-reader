package moreira.vinicius.intreader;

import moreira.vinicius.intreader.controller.FileReadingController;
import moreira.vinicius.intreader.controller.IntegerSearchController;
import moreira.vinicius.intreader.file.IntegersFileReader;
import moreira.vinicius.intreader.file.IntegersFileReaderBuilder;
import moreira.vinicius.intreader.search.BinarySearchAlgorithm;
import moreira.vinicius.intreader.sort.QuickSort;
import moreira.vinicius.intreader.view.CommandLineView;

/**
 * @author vinicius.moreira
 */
public class Application {

	public static void main(String[] args) {
		
		IntegersFileReader fileReader = new IntegersFileReaderBuilder().withNoDuplicates().ordered(new QuickSort()).build();
		
		FileReadingController fileReadingController = new FileReadingController(fileReader);
		IntegerSearchController integerSearchController = new IntegerSearchController(new BinarySearchAlgorithm());
		
		CommandLineView view = new CommandLineView(Files.INTEGERS, fileReadingController, integerSearchController);
		view.initialize();
	}
}
package moreira.vinicius.intreader;

/**
 * @author vinicius.moreira
 */
public class TestFiles {

	public static final String JUST_INTEGERS = "just_integers.txt";
	
	public static final String REPEATED_INTEGERS = "repeated_ints.txt";
	
	public static final String NOT_ONLY_INTEGERS = "not_only_ints.txt";
	
}
package moreira.vinicius.intreader.sort;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.sort.QuickSort;

import org.junit.Test;

/**
 * @author vinicius.moreira
 */
public class QuickSortTest extends AbstractTestSuite {
	
	@Test
	public void mustOrderListWithJust1Element(){
		
		ArrayList<Integer> input = newArrayList(7);
		new QuickSort().execute(input);
		assertThat(input, contains(7));
	}
	
	@Test
	public void mustOrderListWithJust2Elements(){
		
		ArrayList<Integer> input = newArrayList(7,3);
		new QuickSort().execute(input);
		assertThat(input, contains(3,7));
	}
	
	@Test
	public void mustOrderListWithMoreThan2Elements(){
		
		ArrayList<Integer> input = newArrayList(8,9,6);
		new QuickSort().execute(input);
		assertThat(input, contains(6,8,9));
	}
	
	@Test
	public void mustOrderListWithRepeatedIntegers(){
		
		ArrayList<Integer> input = newArrayList(8,9,8,6);
		new QuickSort().execute(input);
		assertThat(input, contains(6,8,8,9));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void mustThrowExceptionForEmptyList(){
		
		ArrayList<Integer> input = new ArrayList<>();
		new QuickSort().execute(input);
	}
	
	@Test(expected = NullPointerException.class)
	public void mustThrowExceptionForNullArgument(){
		new QuickSort().execute(null);
	}
}
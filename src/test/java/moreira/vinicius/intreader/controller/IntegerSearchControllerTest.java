package moreira.vinicius.intreader.controller;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.search.BinarySearchAlgorithm;

/**
 * @author vinicius.moreira
 */
public class IntegerSearchControllerTest extends AbstractTestSuite {

	@Test
	public void testMustTrueForAPresentInteger(){
		IntegerSearchController controller = new IntegerSearchController(new BinarySearchAlgorithm());
		assertTrue(controller.contains(3, newArrayList(1,3,5)));
	}
}
package moreira.vinicius.intreader.controller;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.TestFiles;
import moreira.vinicius.intreader.file.IntegersFileReader;
import moreira.vinicius.intreader.file.IntegersFileReaderBuilder;

/**
 * @author vinicius.moreira
 */
public class FileReadingControllerTest extends AbstractTestSuite {

	@Test
	public void mustReadValidFile(){
		IntegersFileReader reader = new IntegersFileReaderBuilder().simple().build();
		FileReadingController controller = new FileReadingController(reader);
		List<Integer> integers = controller.readFile(TestFiles.JUST_INTEGERS);
		assertThat(integers, not(empty()));
	}
}
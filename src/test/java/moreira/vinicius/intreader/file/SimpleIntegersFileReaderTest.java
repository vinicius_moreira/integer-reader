package moreira.vinicius.intreader.file;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.TestFiles;

/**
 * @author vinicius.moreira
 */
public class SimpleIntegersFileReaderTest extends AbstractTestSuite {

	@Test
	public void mustReadFileWithIntegers(){
		List<Integer> output = new SimpleIntegersFileReader().read(TestFiles.JUST_INTEGERS);
		assertThat(output, containsInAnyOrder(1,3,6,8,9,11,12));
	}
	
	@Test
	public void mustReadFileWithRepeatedIntegers(){
		List<Integer> output = new SimpleIntegersFileReader().read(TestFiles.REPEATED_INTEGERS);
		assertThat(output, contains(8,7,7,8));
	}
	
	@Test(expected = InvalidIntegersFileException.class)
	public void mustThrowExceptionForFileWithNonIntegers(){
		new SimpleIntegersFileReader().read(TestFiles.NOT_ONLY_INTEGERS);
	}
	
	@Test(expected = NullPointerException.class)
	public void mustThrowExceptionWhenTheInputsIsNull(){
		new SimpleIntegersFileReader().read(null);
	}
}
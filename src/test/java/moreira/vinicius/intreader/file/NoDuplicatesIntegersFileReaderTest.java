package moreira.vinicius.intreader.file;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.TestFiles;

import org.junit.Test;

/**
 * @author vinicius.moreira
 */
public class NoDuplicatesIntegersFileReaderTest extends AbstractTestSuite {

	@Test
	public void mustReturnJustDifferentIntegers(){
		List<Integer> integers = new NoDuplicatesIntegersFileReader().read(TestFiles.REPEATED_INTEGERS);
		assertThat(integers, hasSize(2));
		assertThat(integers, contains(8, 7));
	}
}
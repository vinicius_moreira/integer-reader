package moreira.vinicius.intreader.file;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.TestFiles;
import moreira.vinicius.intreader.sort.QuickSort;
import moreira.vinicius.intreader.sort.SortAlgorithm;

/**
 * @author vinicius.moreira
 */
public class OrderedIntegersFileReaderTest extends AbstractTestSuite {

	@Test
	public void mustReturnOrderedList(){
		IntegersFileReader innerReader = new SimpleIntegersFileReader();
		SortAlgorithm sortAlgorithm = new QuickSort();
		
		OrderedIntegersFileReader orderedReader = new OrderedIntegersFileReader(innerReader, sortAlgorithm);
		List<Integer> output = orderedReader.read(TestFiles.JUST_INTEGERS);
		assertThat(output, contains(1,3,6,8,9,11,12));
	}
}
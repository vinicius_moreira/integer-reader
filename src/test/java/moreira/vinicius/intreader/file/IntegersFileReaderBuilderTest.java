package moreira.vinicius.intreader.file;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import moreira.vinicius.intreader.AbstractTestSuite;
import moreira.vinicius.intreader.exception.BuildException;
import moreira.vinicius.intreader.sort.QuickSort;

import org.junit.Test;

/**
 * @author vinicius.moreira
 */
public class IntegersFileReaderBuilderTest extends AbstractTestSuite {

	@Test(expected=BuildException.class)
	public void mustThrowExceptionWhenNoInformationIsProvided(){
		new IntegersFileReaderBuilder().build();
	}
	
	@Test
	public void mustBuildSimpleReaderWhenInformed(){
		IntegersFileReader reader = new IntegersFileReaderBuilder().simple().build();
		assertThat(reader, instanceOf(SimpleIntegersFileReader.class));
	}
	
	@Test
	public void mustBuildOrderedReaderWithTargetAlgorithmWhenProvided(){
		IntegersFileReader reader = new IntegersFileReaderBuilder().ordered(new QuickSort()).build();
		assertThat(reader, instanceOf(OrderedIntegersFileReader.class));
		assertThat(((OrderedIntegersFileReader)reader).getSortAlgorithm(), instanceOf(QuickSort.class));
	}
	
	@Test
	public void mustBuildWithNoDuplicatesReader(){
		IntegersFileReader reader = new IntegersFileReaderBuilder().withNoDuplicates().build();
		assertThat(reader, instanceOf(NoDuplicatesIntegersFileReader.class));
	}
	
	@Test
	public void mustBuildWithNoDuplicatesReaderAfterOrderedCall(){
		IntegersFileReader reader = new IntegersFileReaderBuilder().ordered(new QuickSort()).withNoDuplicates().build();
		assertThat(reader, instanceOf(OrderedIntegersFileReader.class));
		assertThat(((OrderedIntegersFileReader)reader).getInnerReader(), instanceOf(NoDuplicatesIntegersFileReader.class));
	}
	
	@Test(expected = NullPointerException.class)
	public void mustThrowExceptionWhenNoAlgorithmIsSpecifiedForOrdererReader(){
		new IntegersFileReaderBuilder().ordered(null);
	}
	
	@Test(expected = BuildException.class)
	public void mustThrowExceptionWhenOrderedIsCalledMoreThanOnce(){
		new IntegersFileReaderBuilder().ordered(new QuickSort()).ordered(new QuickSort());
	}
}
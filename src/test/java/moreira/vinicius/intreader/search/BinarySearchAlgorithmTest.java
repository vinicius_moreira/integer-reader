package moreira.vinicius.intreader.search;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import moreira.vinicius.intreader.AbstractTestSuite;

import org.junit.Test;

/**
 * @author vinicius.moreira
 */
public class BinarySearchAlgorithmTest extends AbstractTestSuite {

	@Test
	public void mustReturnCorrectIndexForPresentInteger(){
		int indexFound = new BinarySearchAlgorithm().findIndex(newArrayList(1,4,6,7, 11), 4);
		assertThat(indexFound, equalTo(1));
	}
	
	@Test
	public void mustReturnOneNegativeForPresentIntegerInAnUnorderedList(){
		int indexFound = new BinarySearchAlgorithm().findIndex(newArrayList(8,1,12,6,2,3,7), 3);
		assertThat(indexFound, equalTo(-1));
	}
	
	@Test
	public void mustReturnOneNegativeIndexForNotPresentInteger(){
		int indexFound = new BinarySearchAlgorithm().findIndex(newArrayList(1,4,6,7, 11), 2);
		assertThat(indexFound, equalTo(-1));
	}
}

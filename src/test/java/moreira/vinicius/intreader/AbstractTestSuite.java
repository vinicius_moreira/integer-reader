package moreira.vinicius.intreader;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;


/**
 * @author vinicius.moriera
 */
@RunWith(BlockJUnit4ClassRunner.class)
public abstract class AbstractTestSuite {

	protected String getFilePath(String fileName){
		return this.getClass().getResource("/"+fileName).getFile();
	}
}